package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

   // FIXME: You need to write tests for the two known bugs in the code.
   @Test
   public void boxCompleteDetectsCompletedBoxes() {
     // code to test that boxComplete returns true for boxes that *are complete* goes here 
       DotsAndBoxesGrid case1 = new DotsAndBoxesGrid(5, 5, 2);
       case1.drawHorizontal(0, 0, 0);
       case1.drawVertical(0, 0, 1);
       case1.drawHorizontal(0, 1, 0);
       case1.drawVertical(1, 0, 1);
       assertTrue(case1.boxComplete(0, 0));
   }
   @Test
   public void boxCompleteDetectsIncompleteBoxes() {
     // code to test that boxComplete returns false for boxes that *are not complete* goes here
     DotsAndBoxesGrid case2 = new DotsAndBoxesGrid(5, 5, 2);
       case2.drawHorizontal(0, 0, 0);
       case2.drawVertical(0, 0, 1);
       case2.drawHorizontal(0, 1, 0);
       assertFalse(case2.boxComplete(0, 0));
   }
   @Test
   public void drawMethodsDetectRedrawnLines() {
     // code to test that drawHorizontal throws an exception if the line was already drawn goes here
     DotsAndBoxesGrid case3 = new DotsAndBoxesGrid(5, 5, 2);
     case3.drawVertical(0, 0, 1);
      case3.drawHorizontal(0, 0, 1);
      assertAll(
        ()->assertThrows(IllegalStateException.class, ()-> case3.drawVertical(0, 0, 1), "No vertical line has been drawn"),
        ()->assertThrows(IllegalStateException.class, ()-> case3.drawHorizontal(0, 0, 1), "No horizonal line has been drawn")
        );
     
     
   }
}
